#!/usr/bin/env python
#
# Sample RabbitMQ receiver.
#
# This creates a fanout exchange named "exchange", binds a queue named
# "exchange.receiver" to it, and prints any content it receives.
#
# This is largely boilerplate for the pika Python AMQP client library;
# see https://pika.readthedocs.org/ for details.  This uses a
# callback-oriented style where we perform an operation like "declare
# an AMQP exchange", and the pika library performs it and calls a
# callback when the server has sent back an acknowledgement.  As such,
# there are many short not obviously connected functions.
#
# https://pika.readthedocs.io/en/0.10.0/examples/asynchronous_consumer_example.html
# is a more involved (and robust) example.

from __future__ import (
    absolute_import,
    division,
    print_function,
)

from functools import partial

import os

import pika

EXCHANGE = 'exchange'

QUEUE = 'exchange.receiver'


def main():
    amqp_url = os.environ['AMQP_URL']
    print('URL: %s' % (amqp_url,))

    parameters = pika.URLParameters(amqp_url)
    connection = pika.SelectConnection(parameters, on_open_callback=on_open)

    try:
        connection.ioloop.start()
    except KeyboardInterrupt:
        connection.close()
        connection.ioloop.start()


def on_open(connection):
    print('Connected')
    connection.channel(on_channel_open)


def on_channel_open(channel):
    print('Have channel')

    channel.exchange_declare(exchange=EXCHANGE, exchange_type='fanout',
                             durable=True,
                             callback=partial(on_exchange, channel))


def on_exchange(channel, frame):
    print('Have exchange')
    channel.queue_declare(queue=QUEUE, durable=True,
                          callback=partial(on_queue, channel))


def on_queue(channel, frame):
    print('Have queue')

    channel.basic_qos(prefetch_count=1, callback=partial(on_qos, channel))


def on_qos(channel, frame):
    print('Set QoS')
    channel.queue_bind(queue=QUEUE, exchange=EXCHANGE,
                       callback=partial(on_bind, channel))


def on_bind(channel, frame):
    print('Bound')
    channel.basic_consume(queue=QUEUE, consumer_callback=on_message)


def on_message(channel, delivery, properties, body):
    print('Exchange: %s' % (delivery.exchange,))
    print('Routing key: %s' % (delivery.routing_key,))
    print('Content type: %s' % (properties.content_type,))
    print()
    print(body)
    print()

    channel.basic_ack(delivery.delivery_tag)


if __name__ == '__main__':
    main()